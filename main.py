import os
import rpy2.robjects as ro
from rpy2.robjects.conversion import localconverter
from rpy2.robjects.packages import importr
from rpy2.robjects import pandas2ri
from flask import Flask, request

app = Flask(__name__)
@app.route("/ping", methods=['GET'])
def main():
    print(os.getcwd())
    with localconverter(ro.default_converter + pandas2ri.converter):
       ro.r.source("./ping.R")
       ping = ro.r.ping()
       print(ping)
       return {"ping": ping[0]}

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=5000, debug=False)
